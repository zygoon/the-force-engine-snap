# SPDX-License-Identifier: GPL-2.0-only
# SPDX-FileCopyrightText: Zygmunt Krynicki
DESTDIR ?=
bindir = /usr/bin

.PHONY: all clean install

all:
install: $(DESTDIR)$(bindir)/import-gog-data
clean:

$(DESTDIR)$(bindir)/import-gog-data: import-gog-data | $(DESTDIR)$(bindir)
	install -m 755 $< $@
$(DESTDIR)$(bindir):
	mkdir -p $@

