<!--
SPDX-License-Identifier: GPL-2.0-only
SPDX-FileCopyrightText: Zygmunt Krynicki
-->

# Snap package for The Force Engine

This repository contains snapcraft build recipe for The Force Engine.

## Patches

The recipe uses two patches that improve compatibility with snaps:

1) Support for "finding" application files in `$SNAP/usr/share/TheForceEngine`.
2) Support for automatically detecting game data in `$SNAP_USER_COMMON/DarkForces`. 

## Discussion

Please discuss topics related to snap packaging on the snapcraft forum:
https://forum.snapcraft.io/t/the-force-engine-to-play-classics-like-star-wars-dark-forces-in-modern/33799
